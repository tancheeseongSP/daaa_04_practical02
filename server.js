const http = require('http');
const fs = require('fs');
const path = require('path');

const host = 'localhost';
const port = 3000;

const server = http.createServer((req, res) => {

    const { headers, method, url } = req;

    //console.log(headers);
    console.log(`The method is ${method}`);
    console.log(`The url is ${url}`);

    let body = [];

    req.on('error', (err) => {
        console.error(err);
    }).on('data', (chunk) => {
        body.push(chunk);
        //console.log(chunk);
    }).on('end', () => {
        body = Buffer.concat(body).toString();
        console.log(`The body: ${body}`);

        if (method == 'POST') {
            // let myObj = JSON.parse(body);
            // let username = myObj.username;
            // let password = myObj.password;

            let {username, password} = JSON.parse(body);
            console.log(`Username is ${username} and password is ${password}`);

            if (username === 'admin@abc.com' && password === '1234567') { // login is successful
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.end("{'message': 'Welcome admin!'}");
            } else { // login fail
                res.writeHead(301, {Location: 'http://localhost:3000/loginFail.html'});
                res.end();
            }
        } else if (method == 'GET') {
            var fileURL = url;

            if (fileURL == '/') {
                fileURL = '/index.html'
            }
            let filePath = path.resolve('./public' + fileURL);

            fs.stat(filePath, (error, stat) => {
                if (error) {
                    fileUrl = '/error.html';
                    filePath = path.resolve('./public' + fileUrl);
                } else {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'text/html');
                }
                fs.createReadStream(filePath).pipe(res);
            });
        } else {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/html');
            res.write("Welcome");
            res.end();
        }
    });
});


server.listen(port, host, () => {
    console.log(`Server started and accessible via http://${host}:${port}`);
});